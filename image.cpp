#include <stdexcept>
#include <iostream>
#include <cstring>

#include <jpeglib.h>

#include "image.hpp"

//  https://github.com/Windower/libjpeg/blob/master/libjpeg.txt

Image::~Image(){
    delete[] _data;
    _data = nullptr;
}

Image::Image( unsigned int width, unsigned int height, unsigned char n_channels ) :
    _width( width ), _height( height ), _n_channels( n_channels ){

    size_t size = _width * _height * _n_channels;
    _data = new uint8_t[ size ];
}

Image::Image( const Image &other ){
    _width = other._width;
    _height = other._height;
    _n_channels = other._n_channels;
    size_t size = _width * _height * _n_channels;

    if( _data != nullptr )
        delete[] _data;

    _data = new uint8_t[ size ];
    std::memcpy( _data, other._data, size );
}

void Image::operator =( const Image &other ){
    _width = other._width;
    _height = other._height;
    _n_channels = other._n_channels;
    size_t size = _width * _height * _n_channels;

    if( _data != nullptr )
        delete[] _data;

    _data = new uint8_t[ size ];
    std::memcpy( _data, other._data, size );
}

Image Image::toGray(){
    Image new_image( _width, _height, 1 );

    const size_t size = _width * _height;

    uint8_t* const data = new_image.data();
    for( size_t i = 0, j = 0; i < size; i++, j+=3 ){
        data[i] = 0.2989 * _data[j] + 0.5870 * _data[j+1] + 0.1140 * _data[j+2];
    }

    return new_image;
}

//if <forceGrayscale == true> we forcing grayscale, otherwise forcing RGB
void Image::open( std::string const path, bool forceGrayscale ){
    FILE *file = fopen( path.c_str(), "rb" );
    if ( file == nullptr ){
        std::cerr<<"Can't open "<<path<<std::endl;
        exit( 1 );
    }

    jpeg_decompress_struct info;
    jpeg_error_mgr err;

    info.err = jpeg_std_error( &err );
    jpeg_create_decompress( &info );

    jpeg_stdio_src( &info, file );
    jpeg_read_header( &info, true );

    if( forceGrayscale ){
        info.out_color_space = JCS_GRAYSCALE;
    } else {
        info.out_color_space = JCS_RGB;
    }

    jpeg_start_decompress( &info );

    _width = info.output_width;
    _height = info.output_height;

    _n_channels = info.out_color_components;

    size_t dataSize = _width * _height * _n_channels;
    _data = new uint8_t[ dataSize ];
    uint8_t* rowptr;

    while ( info.output_scanline < _height ){
        rowptr = _data + info.output_scanline * _width * _n_channels;;
        jpeg_read_scanlines( &info, &rowptr, 1 );
    }

    jpeg_finish_decompress( &info );
    jpeg_destroy_decompress( &info );
    fclose( file );
}

void Image::save( std::string const path ){
    const int quality = 100;

    jpeg_compress_struct info;
    jpeg_error_mgr err;

    FILE * outfile;
    JSAMPROW row_pointer[1];
    int row_stride;

    info.err = jpeg_std_error( &err );

    jpeg_create_compress( &info );

    if( ( outfile = fopen(path.c_str(), "wb") ) == nullptr ) {
        std::cerr<<"Can't open "<<path<<std::endl;
        exit( 1 );
    }

    jpeg_stdio_dest( &info, outfile );

    info.image_width = _width;
    info.image_height = _height;
    info.input_components = 3;
    info.in_color_space = JCS_RGB;

    jpeg_set_defaults( &info );
    jpeg_set_quality( &info, quality, TRUE );
    jpeg_start_compress( &info, TRUE );

    row_stride = _width * 3;

    while( info.next_scanline < info.image_height ) {
        row_pointer[0] = & _data[info.next_scanline * row_stride];
        (void) jpeg_write_scanlines(&info, row_pointer, 1);
    }

    jpeg_finish_compress(&info);
    fclose(outfile);

    jpeg_destroy_compress(&info);
}

unsigned int Image::width() const{
    return _width;
}

unsigned int Image::height() const{
    return _height;
}

unsigned char Image::channels() const{
    return _n_channels;
}

uint8_t *Image::data(){
    return _data;
}
