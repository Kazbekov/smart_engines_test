#ifndef IMAGE_H
#define IMAGE_H

#include <string>


class Image
{
public:
    Image() = default;
    Image( unsigned int width, unsigned int height, unsigned char n_channels );
    Image( const Image &other );
    void operator =( const Image &other );
    ~Image();
    Image toGray();
    void open( std::string const path, bool forceGrayscale );
    void save( std::string const path );
    unsigned int width() const;
    unsigned int height() const;
    unsigned char channels() const;
    uint8_t* data();

private:
    unsigned int _width;
    unsigned int _height;
    unsigned char _n_channels;

    uint8_t *_data = nullptr;
};

#endif // IMAGE_H
