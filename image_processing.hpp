#ifndef IMAGE_PROCESSING_HPP
#define IMAGE_PROCESSING_HPP

#include <vector>
#include <cmath>
#include <array>

#include "image.hpp"

struct HoughInput{
    std::vector< std::array<int, 2> > points;
    int image_diag;
};

const double pi = std::acos(-1);

double   houghLines( const HoughInput& input, std::vector< std::pair<double, double> > &lines, int linesMax,
                 double rho_step, double theta_step, double min_theta = 0, double max_theta = pi );
void   binarize( Image &src, uint8_t threshold );
//Image  rotate( const Image& src, double angle );
Image  fastRotate( const Image& src, double angle );
void   textToStripes(Image &src);
int    documentResolution( const Image &src );
void   edges( const Image& src, HoughInput& input );
//double computeRotation( std::vector< std::pair<double, double> > &lines );


#endif // IMAGE_PROCESSING_HPP
