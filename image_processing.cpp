#include <iostream>
#include <cstring>
#include <algorithm>
#include <map>
#include <set>
#include <ctime>

#include "image_processing.hpp"

// filter only local maximums in 2D accumulator
void findLocalMaximums( int rho_span, int theta_span, int threshold, const uint32_t *accum, std::vector<uint32_t>& indexes ){
    for( int rho_index = 0; rho_index < rho_span; rho_index++ ){
        for( int theta_index = 0; theta_index < theta_span; theta_index++ ){
            int base = rho_index * theta_span + theta_index;
            if( accum[base] > threshold &&
                accum[base] > accum[base - 1] && accum[base] >= accum[base + 1] &&
                accum[base] > accum[base - theta_span] && accum[base] >= accum[base + theta_span] )
                indexes.push_back(base);
        }
    }
}

// points -> pairs<rho, theta>;
// lines -> pairs<rho, theta> sorted by votes
// return most frequent rotation angle
double houghLines( const HoughInput& input, std::vector< std::pair<double, double> > &lines, int linesMax,
                 double rho_step, double theta_step, double min_theta, double max_theta ){

    lines.clear();
    if( min_theta > max_theta )
        std::swap(min_theta, max_theta);

    const int threshold = input.image_diag * 0.1; // good (strong) line must be voted up by at least <10% image diagonal length> pixels

    const double max_rho = input.image_diag;
    const double min_rho = -max_rho;
    int rho_span = int( (max_rho - min_rho) / rho_step ) + 1;

    int theta_span = std::round( (max_theta - min_theta) / theta_step );

    double sin_table[theta_span];
    double cos_table[theta_span];
    double angle = min_theta;
    for( int i = 0; i < theta_span; i++, angle += theta_step ){
        sin_table[i] = sin( angle );
        cos_table[i] = cos( angle );
    }

    size_t accum_size = rho_span * theta_span;
    auto accum = new uint32_t[ accum_size ];
    memset( accum, 0, accum_size * sizeof(accum[0]) );

    //  accum[rho * theta_span + theta]    +---------> theta
    //                                     |
    //                                     |
    //                                     |
    //                                     V rho

    for( auto point : input.points ){
        for(int n = 0; n < theta_span; n++ ){
            int r = std::round( point[0] * cos_table[n] + point[1] * sin_table[n] );

            int rho_index = int( ( r - min_rho ) / rho_step + 0.5 );
            accum[ rho_index * theta_span + n ]++;
        }
    }

    std::vector<uint32_t> indexes;

    findLocalMaximums( rho_span, theta_span, threshold, accum, indexes );

    std::sort( indexes.begin(), indexes.end(), [&accum](int val_1, int val_2){
        return accum[val_1] > accum[val_2];
    } );

    linesMax = std::min(linesMax, (int)indexes.size());

    std::map<double, int> angle_freq;// sorted associative array <angle, number of votes>

    for( int i = 0; i < linesMax; i++ ){
        double rho = min_rho + int( indexes[i] / theta_span ) * rho_step;
        double theta = min_theta + ( indexes[i] % theta_span ) * theta_step;
        std::pair<double, double> line( rho, theta );
        lines.push_back( line );

        angle_freq[theta] += accum[ indexes[i] ];
    }

    delete[] accum;

    return pi/2 - std::max_element( angle_freq.begin(), angle_freq.end(),
                                    []( const std::pair<double, int>& p1, const std::pair<double, int>& p2 ) {
                                    return p1.second < p2.second;
    })->first;
}

// simple threshold binarization
void binarize( Image &src, uint8_t threshold ){
    size_t size = src.width() * src.height();
    uint8_t* data = src.data();
    for( size_t i = 0; i < size; i++ ){
        if( data[i] < threshold ){
            data[i] = 0;
        } else {
            data[i] = 255;
        }
    }
}

// rotation with linear interpolation
Image fastRotate( const Image& src, double angle ){
//    clock_t begin = clock();

    while( angle < -pi ) angle += 2.0 * pi;
    while( angle > pi ) angle -= 2.0 * pi;

    double cos_value;
    double sin_value;

    if( angle > pi/2.0 ){
        cos_value = std::cos( pi - angle );
        sin_value = std::sin( pi - angle );
    } else {
        cos_value = std::cos( angle );
        sin_value = std::sin( angle);
    }

    const int src_width = src.width();
    const int src_height = src.height();

    const double half_src_width = src_width / 2.0;
    const double half_src_height = src_height / 2.0;

    const int new_width  = std::abs(src_width * cos_value) + std::abs(src_height * sin_value);
    const int new_height = std::abs(src_width * sin_value) + std::abs(src_height * cos_value);

    const double half_new_width_mul_cos = cos_value * new_width / 2.0;
    const double half_new_height_mul_cos = cos_value * new_height / 2.0;
    const double half_new_width_mul_sin = sin_value * new_width / 2.0;
    const double half_new_height_mul_sin = sin_value * new_height / 2.0;

    Image new_image( new_width, new_height, src.channels() );
    memset( new_image.data(), 0, new_width * new_height * new_image.channels() );

    const uint8_t * const src_data = const_cast<Image&>( src ).data();
    uint8_t * const dst_data = new_image.data();

    size_t pos = 0;
    size_t old_pos_1, old_pos_2, old_pos_3, old_pos_4;
    int src_row_step = src_width * 3;

    double row_mul_sin = 0.0;
    double row_mul_cos = 0.0;
    const double K_old_x = half_src_width - half_new_width_mul_cos - half_new_height_mul_sin;
    const double K_old_y = half_src_height + half_new_width_mul_sin - half_new_height_mul_cos;

    for( int row = 0; row < new_height; row++ ){
        double column_mul_sin = 0.0;
        double column_mul_cos = 0.0;

        for( int column = 0; column < new_width; column++ ){
            double old_x = K_old_x + column_mul_cos + row_mul_sin;
            double old_y = K_old_y - column_mul_sin + row_mul_cos;

            if( ( old_x > 0.5 ) && ( old_x < src_width - 1.5 ) && ( old_y > 0.5 ) && ( old_y < src_height - 1.5 ) ){
                for( int channel = 0; channel < 3; channel++ ){
                    double delta_x = old_x - (int)old_x;
                    double delta_y = old_y - (int)old_y;

                    old_pos_1 = ( ceil(old_y)  * src_width + ceil(old_x) ) * 3;

                    old_pos_2 = old_pos_1 - src_row_step;
                    old_pos_3 = old_pos_1 - 3;
                    old_pos_4 = old_pos_2 - 3;

                    double dx_mul_dy = delta_x * delta_y;

                    int value = dx_mul_dy  * src_data[ old_pos_1 + channel ] +
                                (delta_x - dx_mul_dy) * src_data[ old_pos_2 + channel ] +
                                (delta_y - dx_mul_dy)  * src_data[ old_pos_3 + channel ] +
                                (1 - delta_x - delta_y + dx_mul_dy) * src_data[ old_pos_4 + channel ] ;

                    dst_data[pos + channel] = value;
                }


            }
            column_mul_sin += sin_value;
            column_mul_cos += cos_value;

            pos+=3;
        }

        row_mul_sin += sin_value;
        row_mul_cos += cos_value;
    }

//    clock_t end = clock();

//    std::cout << "Fast rotate time spent : " << double(end - begin) / CLOCKS_PER_SEC << std::endl;

    return new_image;
}


// Dilation
void textToStripes( Image &src ){
    int fnt_res = documentResolution( src );

    const int width = src.width();
    const int height = src.height();
    const uint8_t * const src_data = src.data();

    Image temp_image(src);
    uint8_t * const temp_data = temp_image.data();
    memcpy( temp_data, src_data, width * height );

    //precomputing indexes of patch
    std::vector<int> indexes;
    for( int i = -fnt_res; i < fnt_res + 1; i++ ){
        for( int j = -fnt_res; j < fnt_res + 1; j++ ){
            indexes.push_back( i*width + j );
        }
    }

    size_t pos = 0;
    for( int row = fnt_res; row < height - fnt_res; row++ ){
        pos = row * width + fnt_res;
        for( int column = fnt_res; column < width - fnt_res; column++ ){
            if(src_data[pos] == 0){
                for( auto shift : indexes ){
                    temp_data[ pos + shift ] = 0;
                }
            }
            pos++;
        }
    }
    src = temp_image;
}

// most frequent length of white space on a straight line of binarized image
// return value used in dilation algorithm (see textToStripes)
int documentResolution( const Image& src ){
    const int width = src.width();
    const int height = src.height();

    //analizing n_test_strings random rows (not all rows of image, so goes fast)
    const int n_test_strings = 100;
    std::set<int> string_indexes;

    while( string_indexes.size() < n_test_strings + 1 ){
        string_indexes.insert( rand() % height );// [0..height-1]
    }

    std::map<int, int> lengthes;
    size_t total = 0;

    const uint8_t* row_ptr;
    for( int row: string_indexes ){
         row_ptr = const_cast<Image&>( src ).data() + row * width;
         int l_index = 0;
         bool looking_black = true;
         for( int column = 0; column < width; column++ ){
             if( looking_black && ( row_ptr[column] == 0 ) ){
                 l_index = column;
                 looking_black = false;
             } else if( row_ptr[column] != 0 ) {
                 lengthes[column - l_index]++;
                 total++;
                 looking_black = true;
             }
         }
    }

    int resolution = std::max_element( std::begin( lengthes ), std::end( lengthes ),
        [] (const std::pair<int, int> & p1, const std::pair<int, int> & p2) { return p1.second < p2.second; }
    )->first;

    return resolution/2 + 1;
}

// simple edge detector (looking for white->black or black->white edges)
void edges( const Image& src, HoughInput &input ){
    const int height = src.height();
    const int width = src.width();

    input.image_diag = std::hypot( height, width );

    const uint8_t* const data = const_cast<Image&>(src).data();
    size_t pos = width;
    for( int row = 1; row < height; row++ ){
        for( int column = 1; column < width; column++ ){
            if( data[pos] != data[pos-1] || data[pos] != data[pos - width] ){
                input.points.push_back({column, row});
            }
            pos++;
        }
        pos++;
    }
}
