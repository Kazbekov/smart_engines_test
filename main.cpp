//#define USE_OPENCV

#include <iostream>

#include "image.hpp"
#include "image_processing.hpp"

#ifdef USE_OPENCV
#include <opencv2/opencv.hpp>
#endif

int main( int, char** argv ){

#ifdef USE_OPENCV
    cv::namedWindow( "input image" );
    cv::namedWindow( "input gray image" );
    cv::namedWindow( "input binary image" );
    cv::namedWindow( "input striped image" );
    cv::namedWindow( "input edges image" );
    cv::namedWindow( "edges and hough image" );
    cv::namedWindow( "result output image" );
#endif

    Image input_image;
    input_image.open( argv[1], false );

#ifdef USE_OPENCV
    cv::Mat mat_input_image = cv::Mat( input_image.height(), input_image.width(), CV_8UC3, input_image.data() ).clone();
    cv::imshow( "input image", mat_input_image );
#endif

    Image input_gray_image = input_image.toGray();


#ifdef USE_OPENCV
    cv::Mat mat_input_gray_image = cv::Mat( input_gray_image.height(), input_gray_image.width(), CV_8UC1, input_gray_image.data() ).clone();
    cv::imshow( "input gray image", mat_input_gray_image );
    cv::imwrite( "1_input_gray_image.png", mat_input_gray_image);
#endif

    binarize( input_gray_image, 150 );

#ifdef USE_OPENCV
    cv::Mat mat_input_binary_image = cv::Mat( input_gray_image.height(), input_gray_image.width(), CV_8UC1, input_gray_image.data() ).clone();
    cv::imshow( "input binary image", mat_input_binary_image );
    cv::imwrite( "2_input_binary_image.png", mat_input_binary_image);
#endif

    textToStripes( input_gray_image );


#ifdef USE_OPENCV
    cv::Mat mat_input_striped_image = cv::Mat( input_gray_image.height(), input_gray_image.width(), CV_8UC1, input_gray_image.data() ).clone();
    cv::imshow( "input striped image", mat_input_striped_image );
    cv::imwrite( "3_input_striped_image.png", mat_input_striped_image);
#endif

    HoughInput hi;
    edges( input_gray_image, hi );

#ifdef USE_OPENCV
    cv::Mat mat_input_edges_image = cv::Mat( input_gray_image.height(), input_gray_image.width(), CV_8UC1, cv::Scalar(255) );
    for( auto point : hi.points ){
        mat_input_edges_image.at<uint8_t>(point[1], point[0]) = 0;
    }
    cv::imshow( "input edges image", mat_input_edges_image );
    cv::imwrite( "4_input_edges_image.png", mat_input_edges_image );
#endif

    std::vector<std::pair<double, double>> lines;
    double angle = houghLines( hi, lines, 100, 1, pi/180.0 );

#ifdef USE_OPENCV
    cv::Mat mat_input_edges_and_hough_image = mat_input_edges_image.clone();
    for( auto point : hi.points ){
        mat_input_edges_and_hough_image.at<uint8_t>(point[1], point[0]) = 0;
    }
    // Draw the lines
    for( auto line : lines )
    {
        double rho = line.first;
        double theta = line.second;

        cv::Point point_1, point_2;
        double a = cos( theta ), b = sin( theta );
        double x_0 = a * rho;
        double y_0 = b * rho;

        point_1.x = cvRound(x_0 + 2000*(-b));
        point_1.y = cvRound(y_0 + 2000*(a));
        point_2.x = cvRound(x_0 - 2000*(-b));
        point_2.y = cvRound(y_0 - 2000*(a));

        cv::line( mat_input_edges_and_hough_image, point_1, point_2, cv::Scalar(127), 1, cv::LINE_AA);
    }

    cv::imshow( "edges and hough image", mat_input_edges_and_hough_image );
    cv::imwrite( "7_edges_and_hough_image.png", mat_input_edges_and_hough_image );
#endif

    std::cout<<"Rotation = "<<angle*180.0/pi<<std::endl;

    angle = houghLines( hi, lines, 100, 1, ( pi/180.0 ) * 0.01, pi/2.0 - (angle - pi*1.5/180.0) , pi/2.0 - (angle + pi*1.5/180.0) ); // +\- 1.5 degree
    std::cout<<"Precize rotation = "<<angle*180.0/pi<<std::endl;

    Image result_image = fastRotate( input_image, angle );
    result_image.save( "result.jpg" );

#ifdef USE_OPENCV
    cv::Mat output_colour_image = cv::Mat( result_image.height(), result_image.width(), CV_8UC3, result_image.data() ).clone();
    cv::cvtColor(output_colour_image, output_colour_image, cv::COLOR_RGB2BGR);
    cv::imshow( "result output image", output_colour_image );

    cv::waitKey();
#endif
    return 0;
}
